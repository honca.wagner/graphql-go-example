package main

import "github.com/graphql-go/graphql"

// Participant ...
type Participant struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Type int    `json:"type"`
}

// Event ...
type Event struct {
	ID           string        `json:"id"`
	Participants []Participant `json:"participants"`
	Tournament   Tournament    `json:"tournament"`
}

// Tournament ...
type Tournament struct {
	ID     string  `json:"id"`
	Name   string  `json:"name"`
	Events []Event `json:"events"`
}

// ParticipantGqlType ...
var ParticipantGqlType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Participant",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.String,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
			"type": &graphql.Field{
				Type: graphql.Int,
			},
		},
	},
)

// EventGqlType ...
var EventGqlType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Event",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.String,
			},
			"participants": &graphql.Field{
				Type: graphql.NewList(ParticipantGqlType),
			},
		},
	},
)

// TournamentGqlType ...
var TournamentGqlType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Tournament",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.String,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
		},
	},
)

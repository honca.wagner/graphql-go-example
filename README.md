# Simple GraphQL server in Go

## Run
```
docker-compose build
docker-compose up -d
```

Server is now running at http://localhost:3000/graphql.

## Examples of queries with HTTP POST
### Participants
```
curl --data '{"query": "{event {id, participants {id, name, type}, tournament {id, name}}}"}' http://localhost:3000/graphql

curl --data '{"query": "{event(id: \"SUplTVOB\") {id, participants {id, name, type}, tournament {id, name}}}"}' http://localhost:3000/graphql
```
### Events
```
curl --data '{"query": "{participant {id, name, type}}"}' http://localhost:3000/graphql

curl --data '{"query": "{participant(id: \"4fGZN2oK\") {id, name, type}}"}' http://localhost:3000/graphql
```
### Tournaments
```
curl --data '{"query": "{tournament {id, name, events {id, tournament {id}}}}"}' http://localhost:3000/graphql

curl --data '{"query": "{tournament(id: \"6N6d2yER\") {id, name, events {id, tournament {id}}}}"}' http://localhost:3000/graphql
```
## Benchmark using ab
```
ab -T 'application/x-www-form-urlencoded' -n 20000 -c 100 -c 10 -p benchmark.data http://localhost:3000/graphql
```
## GraphQL libraries
* https://github.com/graphql-go/graphql - used in this example
* https://github.com/graph-gophers/graphql-go
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/graphql-go/graphql"
)

type reqBody struct {
	Query string `json:"query"`
}

var participants []Participant
var tournaments []Tournament
var events []Event

func main() {
	TournamentGqlType.AddFieldConfig("events", &graphql.Field{Type: graphql.NewList(EventGqlType)})
	EventGqlType.AddFieldConfig("tournament", &graphql.Field{Type: TournamentGqlType})

	assignDataFromFile("participants.json", &participants)
	assignDataFromFile("events.json", &events)
	assignDataFromFile("tournaments.json", &tournaments)

	http.Handle("/graphql", gqlHandler())
	log.Fatal(http.ListenAndServe(":3000", nil))
}

func gqlHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Body == nil {
			http.Error(w, "No query data", 400)
			return
		}

		var rBody reqBody
		err := json.NewDecoder(r.Body).Decode(&rBody)
		if err != nil {
			http.Error(w, "Error parsing JSON request body", 400)
		}

		fmt.Fprintf(w, "%s", processQuery(rBody.Query))
	})
}

func processQuery(query string) (result string) {
	params := graphql.Params{Schema: gqlSchema(), RequestString: query}
	r := graphql.Do(params)
	if len(r.Errors) > 0 {
		fmt.Printf("failed to execute graphql operation, errors: %+v", r.Errors)
	}
	rJSON, _ := json.Marshal(r)

	return fmt.Sprintf("%s", rJSON)

}

func assignDataFromFile(filename string, data interface{}) {
	jsonf, err := os.Open(filename)

	if err != nil {
		fmt.Printf("failed to open %s file, error: %v", filename, err)
	}

	jsonDataFromFile, _ := ioutil.ReadAll(jsonf)
	defer jsonf.Close()

	err = json.Unmarshal(jsonDataFromFile, &data)

	if err != nil {
		fmt.Printf("failed to parse %s, error: %v", filename, err)
	}
}

// Define the GraphQL Schema
func gqlSchema() graphql.Schema {
	fields := graphql.Fields{
		"participant": &graphql.Field{
			Type:        graphql.NewList(ParticipantGqlType),
			Description: "Get all participants.",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				id, success := params.Args["id"].(string)
				if success {
					for _, participant := range participants {
						if participant.ID == id {
							return []Participant{participant}, nil
						}
					}
				}
				return participants, nil
			},
		},
		"event": &graphql.Field{
			Type:        graphql.NewList(EventGqlType),
			Description: "Get all events.",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				id, success := params.Args["id"].(string)
				if success {
					for _, event := range events {
						if event.ID == id {
							return []Event{event}, nil
						}
					}
				}
				return events, nil
			},
		},
		"tournament": &graphql.Field{
			Type:        graphql.NewList(TournamentGqlType),
			Description: "Get all tournaments.",
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				id, success := params.Args["id"].(string)
				if success {
					for _, tournament := range tournaments {
						if tournament.ID == id {
							return []Tournament{tournament}, nil
						}
					}
				}
				return tournaments, nil
			},
		},
	}

	rootQuery := graphql.ObjectConfig{Name: "RootQuery", Fields: fields}
	schemaConfig := graphql.SchemaConfig{Query: graphql.NewObject(rootQuery)}
	schema, err := graphql.NewSchema(schemaConfig)
	if err != nil {
		fmt.Printf("failed to create new schema, error: %v", err)
	}

	return schema
}
